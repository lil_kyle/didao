# Didao

A very, very simple gopher server. Requires a directory with gopher-servable files.

```ruby
require 'didao'

Didao.new.run
```

This looks in the present working directory for a Gophermap and other servable
files. Because it broadcasts to Gopher-standard port 70 by default, you will
probably need to run it with root privileges.

If you want to choose a port and a path:

```ruby
require 'didao'

Didao.new(3333, '/home/kyle/gopher/').run
```

Please note that the way Gophermaps handle relative paths means that you
generally can't run any Gophermap from any path willy-nilly.

## Installation

In your Gemfile:

```ruby
gem 'didao'
```
or on the console:

```ruby
$ gem install didao
```

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/surdegg/didao. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

This project was created by [Kyle Church](https://kylechur.ch) at [Surd Egg](https://surdegg.com).
