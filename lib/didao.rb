# frozen_string_literal: true

require 'socket'

# A class representing a single gopher server instance
class Didao
  def initialize(port = 70, path = Dir.pwd)
    @port = port
    @path = path
    @path += '/' unless @path =~ %r{\/$}
    @server = TCPServer.open(port)
  end

  def process_file(file_name)
    file_name = file_name.sub(%r{^\/.\/}, '').gsub(/^\//, '')
    file_name = 'gophermap' if file_name.empty?

    File.open(@path + file_name, 'rb').read.split("\n").map do |l|
      l = 'i' + l unless l[0] =~ /[0-9]/
      p l
    end.join("\n")
    # This is technically nonstandard, as gophermaps should always end with '.'
    # This is best remedied outside this class, which should just be basic
    # server operations.
  rescue Errno::ENOENT => e
    puts e.message
    'Not Found'
  end

  def handle_request(request, source)
    gopher_path = request.gsub("\r\n", '<CR><LF>').gsub(/^\//, '')
    puts gopher_path
    puts '---'
    puts "#{Time.now}: Received request: \"#{gopher_path}\" from #{source}"
    file = request == "\r\n" ? 'gophermap' : request.chomp
    puts "#{Time.now}: Returning requested file: \"#{file}\""
    process_file(file)
  end

  def run
    puts "Didao listening on #{@port}. Press Ctrl+C to exit."
    loop do
      client = @server.accept
      Thread.start(client) do |conn|
        conn.puts handle_request(conn.readline, conn.peeraddr[3])
        conn.close
      end
    end
  rescue Interrupt
    puts "\nClosing Didao Server..."
  end
end
