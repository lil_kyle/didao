#! /usr/bin/env ruby

# frozen_string_literal: true

# By default, this server runs on port 70 and the present working directory.
# To change this, try running as:
#
# ./elaborate.rb -p 3333 -d /path/to/file

require 'optparse'
require_relative '../lib/didao'

port = 70
directory = Dir.pwd

OptionParser.new do |parser|
  parser.on('-p', '--port [NUMBER]', 'Set the port') do |p|
    Kernel.abort('The \'--port\' flag requires a port number.') unless p
    port = p
  end

  parser.on('-d', '--dir [DIRECTORY]', 'Set the directory') do |d|
    Kernel.abort('The \'--dir\' flag requires a directory argument.') unless d
    directory = d
  end
end.parse!

Didao.new(port, directory).run
