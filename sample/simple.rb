#! /usr/bin/env ruby

# frozen_string_literal: true

# ATTENION: Didao uses PWD by default, so this very simple sample needs to be
# run in the same directory as itself and the gopher map.
#
# Because it uses Gopher's port 70 by default, you will likely need to run it
# with admin privileges.

require_relative '../lib/didao'

Didao.new.run
