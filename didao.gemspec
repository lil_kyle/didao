# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'didao'
  s.version = '0.1.0'
  s.date = '2019-07-15'
  s.summary = 'A very simple gopher server.'
  s.description = 'A very simple gopher server, which can be use to get a\
    simple gopher site up quickly, or to build a better server.'
  s.authors = ['Kyle Church']
  s.email = 'kyle.church@surdegg.com'
  s.files = ['lib/didao.rb']
  s.homepage = 'https://bitbucket.org/SurdEgg/didao/src/master/'
  s.license = 'MIT'
end
